function loadCount(elementID, pre, post) {

    bounty.default({
        el: elementID,
        value: pre + '00,000' + post,
        initialValue: '0',
        lineHeight: 1.35,
        letterSpacing: 1,
        animationDelay: 500,
        letterAnimationDelay: 100
    });

    $.get("https://api.purecore.io/rest/1/metrics/players/unique/?key=10997ff01578fb86e266c704795fc11cb703bda3da7ab82f6b83efa8e7041977", function (json) {

        count = json.count
        finalCount = parseInt(count) + 22801

        bounty.default({
            el: elementID,
            value: pre + finalCount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + post,
            initialValue: '0',
            lineHeight: 1.35,
            letterSpacing: 1,
            animationDelay: 100,
            letterAnimationDelay: 100
        });

    });

}

function loadPlayTime(elementID, pre, post) {

    $.get("https://api.purecore.io/rest/1/metrics/playtime/network/?key=10997ff01578fb86e266c704795fc11cb703bda3da7ab82f6b83efa8e7041977", function (json) {

        count = json.count

        var seconds = parseInt(count, 10);
        var days = Math.floor(seconds / (3600 * 24));
        seconds -= days * 3600 * 24;
        var hrs = Math.floor(seconds / 3600);
        seconds -= hrs * 3600;
        var mnts = Math.floor(seconds / 60);
        seconds -= mnts * 60;

        $("#counterSection").removeClass("hidden");
        $("#counterSection").addClass("animated tdFadeInDown")

        bounty.default({
            el: elementID,
            value: pre + days + " days, " + hrs + " Hrs, " + mnts + " Minutes, " + seconds + " Seconds" + post,
            initialValue: '0',
            lineHeight: 1.35,
            letterSpacing: 1,
            animationDelay: 100,
            letterAnimationDelay: 100
        });

    });

}

function openUserInfo(open) {
    if (open) {
        $(".userInfo").attr("open", true);
    } else {
        $(".userInfo").attr("open", null);
    }
}

function toggleUserInfo() {
    var attr = $(".userInfo").attr('open');
    if (typeof attr !== typeof undefined && attr !== false) {
        $(".userInfo").attr("open", null)
    } else {
        $(".userInfo").attr("open", true)
    }
}

function getSessionCard(session) {

    const element = document.createElement('div');
    element.classList.add("sessionCard");
    element.innerHTML = '<i class="avatar" style="background-image: url(https://minotar.net/avatar/' + session.getPlayer().getUsername() + ')"></i><p class="name">' + session.getPlayer().getUsername() + '</p>'
    return element;

}

function showDiscordInfo(offlineSession) {
    $("#account-session-selection").addClass("animated tdShrinkOut")
    setTimeout(() => {
        $("#account-session-selection").addClass("hidden")
        $("#account-discord-linking").removeClass("hidden")
        $("#account-discord-linking").addClass("animated tdExpandIn")
    }, 200);
}

function linkDiscord(sessionRequest, offlineSession) {

    // discord login

    var win = window
    const y = win.top.outerHeight / 2 + win.top.screenY - (600 / 2);
    const x = win.top.outerWidth / 2 + win.top.screenX - (400 / 2);
    var newWin = win.open(sessionRequest.getValidationUrl(), "purecore discord linking", 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + 400 + ', height=' + 600 + ', top=' + y + ', left=' + x);

    var timer = setInterval(function () {
        if (newWin.closed) {

            clearInterval(timer);
            saveSession(offlineSession)

        }
    }, 200);

}

function saveSession(offlineSession) {

    localStorage.setItem("playerSessionUuid", offlineSession.getId());
    localStorage.setItem("playerSessionHash", offlineSession.getHash());
    openUserInfo(false);
    getAccounts();

}

function loadSessionDetails() {
    // to do
    var CoreInstance = new Core("10997ff01578fb86e266c704795fc11cb703bda3da7ab82f6b83efa8e7041977")
    var SessionObj = new Session(CoreInstance)
    SessionObj.fromHash(localStorage.getItem("playerSessionHash")).then(function (session) {

        var player = session.getPlayer()
        var name = player.getUsername()
        var uuid = player.getUuid()
        var core = player.getId()

        $(".selectedAccount").text(name)
        $(".selectedAccountPlain").text(name)
        $("#account-info").removeClass("hidden")

    }).catch(function (error) {

        logoff()

    })
}

function getPunishmentRow(punishment) {

    var status = punishment.getStatus().toString();

    var icon = "help_outline";

    if (status == "open") {
        icon = "cancel_schedule_send"
    } else if (status == "replied") {
        icon = "gavel"
    } else if (status == "waiting_reply") {
        icon = "chat_bubble"
    }

    var cList = ""
    var offenceList = punishment.getOffenceList();
    for (let index = 0; index < offenceList.length; index++) {
        const offence = offenceList[index];
        if (index == offenceList.length - 1) {
            // last item, no comma
            cList += offence.getName();
        } else {
            cList += offence.getName() + ", ";
        }
    }

    return "<tr><td>" + punishment.getPlayer().getUsername() + "</td><td>" + cList + "</td><td>" + punishment.getPoints("GMP") + "</td><td>" + punishment.getPoints("CHT") + "</td><td><i class=\"material-icons\">" + icon + "</i></td></tr>"
}

function loadPunishmentList(element) {

    element.empty()

    var CoreInstance = new Core("10997ff01578fb86e266c704795fc11cb703bda3da7ab82f6b83efa8e7041977")
    CoreInstance.getInstance().asNetwork().getPunishments().then(function (punishmentList) {

        punishmentList.forEach(punishment => {
            element.append(getPunishmentRow(punishment));
        });

    })
}

function getOffenceCard(offence) {
    return "<div style=\"margin-bottom: 10px\"><p style=\"margin: 0px\" class=\"subsub\">" + offence.getName() + " <span style=\"color:#d4441a\">-" + offence.getNegativePoints() + "</span></p><span style=\"color: white\">" + offence.getDescription() + "</span></div>"
}

function loadOffenceList(gameplayContainer, chatContainer) {

    var CoreInstance = new Core("10997ff01578fb86e266c704795fc11cb703bda3da7ab82f6b83efa8e7041977")
    CoreInstance.getInstance().asNetwork().getOffences().then(function (offenceList) {

        offenceList.forEach(offence => {
            if (offence.getType() == "GMP") {
                gameplayContainer.append(getOffenceCard(offence))
            } else {
                chatContainer.append(getOffenceCard(offence))
            }
        });

    })

}

function logoff() {
    localStorage.removeItem("playerSessionUuid")
    localStorage.removeItem("playerSessionHash")
    $(".selectedAccount").text("Unknown Player")
    $(".selectedAccountPlain").text("Unknown Player")
    getAccounts()
}

function getOfflineSession(element, hash) {

    element.setAttribute("loading", "true");

    var CoreInstance = new Core("10997ff01578fb86e266c704795fc11cb703bda3da7ab82f6b83efa8e7041977") // starts a core instance

    // requests offline session for the session linked to the element
    hash.requestSession().then(function (sessionRequest) {

        // if it is validated by default, save it
        // should show warning to user as the account is not secured with discord
        if (sessionRequest.isValidated()) {

            element.removeAttribute("loading")
            element.setAttribute("unlocked", "true");

            setTimeout(() => {

                element.removeAttribute("unlocked")
                element.setAttribute("gettingKey", "true")

                sessionRequest.getSession().then(function (offlineSession) {

                    element.removeAttribute("gettingKey")
                    element.setAttribute("key", "true")

                    showDiscordInfo(offlineSession)

                    $("#discordLink").click(function () {
                        linkDiscord(sessionRequest, offlineSession)
                    });

                })
            }, 100);

            // if it isn't, auth with discord and then save it
        } else {

            element.removeAttribute("loading")
            element.setAttribute("locked", "true");

            // active checker

            var win = window
            const y = win.top.outerHeight / 2 + win.top.screenY - (600 / 2);
            const x = win.top.outerWidth / 2 + win.top.screenX - (400 / 2);
            var newWin = win.open(sessionRequest.getValidationUrl(), "purecore discord linking", 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + 400 + ', height=' + 600 + ', top=' + y + ', left=' + x);

            var timer = setInterval(function () {
                if (newWin.closed) {

                    clearInterval(timer);
                    element.removeAttribute("loading")
                    element.setAttribute("gettingKey", "true")

                    sessionRequest.getSession().then(function (session) {

                        element.removeAttribute("gettingKey")
                        element.setAttribute("key", "true")
                        saveSession(session)

                    }).catch(function (error) {

                        element.removeAttribute("gettingKey")
                        element.setAttribute("locked", "true");

                    });
                }
            }, 200);

        }

    })

}

getAccounts()
function getAccounts() {

    $("#live-accounts").empty()
    $("#account-info").addClass("hidden")
    $("#account-discord-linking").addClass("hidden")
    $("#no-accounts-found").addClass("hidden")

    if (localStorage.getItem("playerSessionUuid") == null || localStorage.getItem("playerSessionHash") == null) {
        var CoreInstance = new Core("10997ff01578fb86e266c704795fc11cb703bda3da7ab82f6b83efa8e7041977") // starts a core instance
        CoreInstance.getInstance().asNetwork().getHashes().then(function (hashes) {

            if (hashes.length > 0) {
                toggleUserInfo()
                $("#account-session-selection").removeClass("hidden");
            } else {
                $("#no-accounts-found").removeClass("hidden");
            }

            hashes.forEach(hash => {
                var element = getSessionCard(hash);
                $("#live-accounts").append(element)
                element.addEventListener("click", function () {
                    getOfflineSession(element, hash)
                }, false);
            });

        }).catch(function (e) {
            // ignore
        })
    } else {
        loadSessionDetails()
        $("#account-session-selection").addClass("hidden")
    }

}



function loadAdvancements() {

    var session = new CoreOfflineSession(localStorage.getItem("playerSessionUuid"), localStorage.getItem("playerSessionHash"))
    session.getPlayerData().then(function (coreSession) {

        coreSession.setCoreKey("10997ff01578fb86e266c704795fc11cb703bda3da7ab82f6b83efa8e7041977")
        coreSession.getAdvancements().then(function (advdata) {

            console.log(advdata);

        })

    })

}
